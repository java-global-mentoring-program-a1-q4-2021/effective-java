package effective.java;

import java.util.HashMap;
import java.util.LinkedHashSet;

public class LFUCache {
    private final HashMap<Integer, String> values;
    private final HashMap<Integer, Integer> counts;
    private final HashMap<Integer, LinkedHashSet<Integer>> lists;
    private final int size;
    private int min = -1;

    public LFUCache(int size) {
        this.size = size;
        this.values = new HashMap<>();
        this.counts = new HashMap<>();
        this.lists = new HashMap<>();
        this.lists.put(0, new LinkedHashSet<>());
    }

    public void put(int key, String value) {
        if (size <= 0) {
            return;
        }
        if (values.containsKey(key)) {
            values.put(key, value);
            get(key);
            return;
        }
        if (values.size() >= size) {
            int evict = lists.get(min).iterator().next();
            lists.get(min).remove(evict);
            values.remove(evict);
            counts.remove(evict);
        }
        values.put(key, value);
        counts.put(key, 0);
        min = 0;
        lists.get(0).add(key);
    }

    public String get(int key) {
        if (!values.containsKey(key)) {
            return null;
        }
        int count = counts.get(key);
        counts.put(key, count + 1);
        lists.get(count).remove(key);
        if (count == min && lists.get(count).isEmpty()) {
            min++;
        }
        if (!lists.containsKey(count + 1)) {
            lists.put(count + 1, new LinkedHashSet<>());
        }
        lists.get(count + 1).add(key);
        return values.get(key);
    }

    public int getCacheSize() {
        return values.size();
    }
}
