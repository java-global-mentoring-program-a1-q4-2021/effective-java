package effective.java;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;

import java.util.concurrent.TimeUnit;

public class LRUCache {
    private final Cache<Integer, String> cache;

    public LRUCache(long size, long liveTimeInSecond) {
        this.cache = CacheBuilder.newBuilder()
                .maximumSize(size)
                .expireAfterWrite(liveTimeInSecond, TimeUnit.SECONDS)
                .removalListener((RemovalListener<Integer, String>) removal ->
                        System.out.println("{" + removal.getKey() + ":" + removal.getValue() + "} was removed"))
                .build();
    }

    public void put(Integer key, String value) {
        cache.put(key, value);
    }

    public String get(Integer key) {
        return cache.getIfPresent(key);
    }

    public long getCacheSize() {
        return cache.size();
    }
}
