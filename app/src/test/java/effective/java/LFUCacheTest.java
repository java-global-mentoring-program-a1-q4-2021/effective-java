package effective.java;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class LFUCacheTest {

    @Test
    void testEvictionBySize() {
        LFUCache cache = new LFUCache(3);
        cache.put(1, "aa");
        cache.put(2, "bb");
        cache.put(3, "cc");
        cache.put(4, "dd");
        assertEquals(3, cache.getCacheSize());
        assertEquals("bb", cache.get(2));
        assertEquals("cc", cache.get(3));
        assertEquals("dd", cache.get(4));
        assertNull(cache.get(1));
    }
}

