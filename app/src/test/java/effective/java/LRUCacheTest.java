package effective.java;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class LRUCacheTest {

    @Test
    void testEvictionBySize() {
        LRUCache cache = new LRUCache(3, 10L);
        cache.put(1, "aa");
        cache.put(2, "bb");
        cache.put(3, "cc");
        cache.put(4, "dd");
        assertEquals(3, cache.getCacheSize());
        assertEquals("bb", cache.get(2));
        assertEquals("cc", cache.get(3));
        assertEquals("dd", cache.get(4));
        assertNull(cache.get(1));
    }

    @Test
    void testEvictionByEntryLiveTimeExpire() throws InterruptedException {
        LRUCache cache = new LRUCache(3,3L);
        cache.put(1, "aa");
        cache.put(2, "bb");
        cache.put(3, "cc");
        assertEquals(3, cache.getCacheSize());
        Thread.sleep(3500);
        cache.put(4, "dd");
        assertEquals(1, cache.getCacheSize());
        assertEquals("dd", cache.get(4));
        assertNull(cache.get(1));
        assertNull(cache.get(3));
        assertNull(cache.get(3));
    }
}

